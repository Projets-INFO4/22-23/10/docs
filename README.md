# Documentation Mini Compiler C :

This projet is a mini compiler made for the `Mini Processor MCPU` (See documentation here) [FR](./Documentation-FR/MCPU-Explications.md) : [EN](./Documentation-EN/MCPU-Explainations.md)

The goal of this projet is to manage to compile a sub set of the C programming language into the `ASM Assembly` which the MCPU uses (See documentation here) [FR](./Documentation-FR/Doc-MCPU-Architecture.md) : [EN](./Documentation-EN/Doc-MCPU-Architecture.md)  

In order to achieve such a goal we created our subset of the C programming language using a specif Grammar [(See here)](Grammar_Subset_C.md)

---

- ## [Link of the code of the mini compiler](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/)
- ## [Logbook's link](Logbook_mini_compiler_info4_2022_2023.md)
- ## [Link of the Logbook out of session](Logbook_out_of_sessions.md)
- ## Link of Mid term presentation - [FR](./Soutenance_Mi-parcours_FR.pdf) - [EN](./Soutenance%20Mi-parcours_EN.pdf)
- ## [Link of the Final presentation](./presentation.pdf)
- ## [FINAL REPORT](./report.pdf)
---

All the documentations are written both in French and English.

They can be found respectively in the [Documentation-FR](./Documentation-FR/) and [Documentation-EN](./Documentation-EN/)

To compile the project: make 
To launch the project in demo mode : 
- go on demo git branch 
  - use ./main -t to launch the lexer demo
- In global : ./main FileTOCompile && python ScriptPython/asm.py < out.asm | python ScriptPython/emu.py 2000
  - with ./main FileTOCompile runs the compiler and generates the asm code
  - python ScriptPython/asm.py < out.asm convert ASM to binary
  - python ScriptPython/emu.py 2000 emulates the MCPU

