# Documentation MCPU - GitHub

- [Documentation MCPU - GitHub](#documentation-mcpu---github)
  - [Short description of MCPU](#short-description-of-mcpu)
  - [Cardboard's Architecture](#cardboards-architecture)
  - [The instructions](#the-instructions)
  - [Macros](#macros)
  - [Exemple of commented ASM Assembly code](#exemple-of-commented-asm-assembly-code)
  - [Control part](#control-part)
  - [Problems](#problems)
  - [Proposed solution:](#proposed-solution)

## Short description of MCPU

The MCPU is a processor that was designed to run on a 32 Macrocell CPLD(link), one of the smallest programmable logic tools.

It is not intended to run in real conditions, in fact it would be incapable of doing so.

This card is 8 bits and uses 2 bits for instructions, so it only has 2^6 so 64 bytes of RAM.

## Cardboard's Architecture

The CPU follow this description :
```basic
entity CPU8BIT2 is
port (
    data: inout std_logic_vector(7 downto 0);
    adress: out std_logic_vector(5 downto 0);
    oe: out std_logic;
    we: out std_logic;
    rst: in std_logic;
    clk: in std_logic);
end;
```

- The CPU has 8 bits for data that is both input and output.
- The CPU has 6 bits for addresses that are output only.
- oe and we are output-only flags.
- rst and clk are input only flags.


## The instructions

| Mnemonic | Code |    Opcode    |
|:--------:|:----:|:------------:|
|   NOR    |  00  | [CODE]AAAAAA |
|   ADD    |  01  | [CODE]AAAAAA |
|   STA    |  10  | [CODE]AAAAAA |
|   JCC    |  11  | [CODE]DDDDDD |

![MCPU's Architecture](./../images/Architecture.png)

- There is actually only one hardware register named Akku 
    - NOR performs the NOR operation between the data in Akku and the data in the AAAAAA memory space and stores it in Akku
    - ADD performs the operation ADD between the data located in AKKU and the data located in the memory space AAAAAA and the stock in Akku and updates C(1 if the result is positive 0 otherwise)
    - STA sets the data located in Akku to address AAAAAA
    - JCC updates the PC if C is at 1 with address DDDDDD and resets C to 0
- More complex instructions can be made by mixing basic instructions


## Macros

As the processor only has 2 bits for instructions, it only has 2^2 so 4 instructions max (see above).

There are therefore macros which are sequences of assembler functions which execute a specific function.

|   Macro   |             Assembly Code             |
|:---------:|:--------------------------------------:|
|    CLR    |             **NOR** allone             |
| LDA [mem] |      **NOR** allone, **ADD** mem       |
|    NOT    |              **NOR** zero              |
| JMP [dst] |        **JCC** dst, **JCC** dst        |
| JCS [dst] |       **JCC** \*+2, **JCC** dst        |
| SUB [mem] | **NOR** zero, **ADD** mem, **ADD** one |

**allone = 0xFF**
**zero = 0x00**
**one = 0x01**

- CLR allows you to clear or empty the accumulator (Akku)
- LDA [mem] allows you to load mem into the accumulator (Akku)
- NOT allows to invert the content of the accumulator (Akku)
- JMP [dst] allows to jump to the destination in all cases
- JCS [dst] jumps if the outgoing carry is true.
- SUB [mem] allows you to add the mem value to the accumulator (Akku)

## Exemple of commented ASM Assembly code

```arm
start :
    NOR allone              ; Akku = 0 => CLR !
    NOR b                   ; NOT !
    ADD one                 ; Akku = − b
    ADD a                   ; Akku = a − b
                            ; Carry set when akku >= 0
    JCC neg
    STA a
    ADD allone
    JCC end                 ;A=0 ? −> end , result in b
    JCC start
neg :
    NOR zero                ; NOT !
    ADD one                 ; Akku = −Akku
    STA b
    JCC start               ; Carry was not altered
end :
    JCC end
```

This program calculates the gcd of two numbers a and b using the Dijkstra technique.

## Control part

The CPU uses a 5-state summer machine. Here is a schematic representing :

![Control part](./../images/PC.png)

S0 = 000
S1 = 001
S2 = 010
S3 = 011
S5 = 101

## Problems

With 64 bytes of RAM we don't have the space to make large C programs because we would run out of space to store the code.

Also the CPU has no registers which means that the RAM will have to store all the data.

So in 8 bits the CPU is extremely limited and has many constraints that we need to solve in order to run a C compiler on it.

## Proposed solution:

In order to solve these problems we have several solutions:
- Increase the CPU size from 8 bits to 16 or 32 bits for example. This would increase the RAM considerably and would allow to have enough storage space to be able to do everything.

**OR**

- Increase the size of the CPU dynamically. Using FPGA technology to dynamically change the size of the CPU. In this case the board will have :
    - n input/output bits for data
    - n-2 output bits for address
