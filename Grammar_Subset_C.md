# Grammaire Small C

---
## CONSTANT TOKEN
---

- [ ] **CONSTANTES_COMP** = ">" | "<" | "==" | "<=" | ">="

- [ ] **CONSTANTES_LOGIQUE_UNAIRE** = "!" | EPSILON

- [ ] **CONSTANTES_LOGIQUE_DUAL** = "||" | "&&" | EPSILON

- [ ] **KEY_WORD** = "for" | "while" | "if" | "else" | "return" | "do" | "goto"

- [ ] **KEY_WORD_LOOP** = "break" | "continue" 

- [ ] **TYPES** = ("char" | "int" | "unsigned int" | "void") . ("\*" | "[]" | EPSILON)

- [ ] **PRE_PROCESSING** = "#define" . NOM . String

- [ ] **OPERATORS** = "+" | "-" | "\*" | "/" | "%"

---
## SYNTAX RULES
---

- [ ] **GRAMMAR** = PRE_PROCESSING | INSTRUCTION | DECLARATION_FONCT | MAIN | GRAMMAR_PLUS

- [ ] **GRAMMAR_PLUS** = GRAMMAR | EPSILON

- [ ] **MAIN** = TYPES . "main" . "(" . PARAMOUVIDE . ")" . "{" . CORPS . "}"

- [ ] **CORPS** = SUITE_INSTRUCTION | EPSILON

- [ ] **CORPS_LOOP** = SUITE_INSTRCTION | KEYWORD_LOOP.";".CORPS_LOOP | EPSILON

- [ ] **SUITE_INSTRUCTION** = (INSTRUCTION . ";" SUITE_INSTRUCTION) | EPSILON

- [ ] **INSTRUCTION** = DECLARATION_VAR | APPEL_FONC | LOOP | IF_STATEMENT | RETURN_INSTRUCTION | AFFECTATION_VAR

---
## CONDTION BLOCK
---

- [ ] **IF_STATEMENT** = "if (" . CONDITIONS . "){" . CORPS . "}" . ELSE_STATEMENT 

- [ ] **ELSE_STATEMENT** = "else {" . CORPS . "}" | EPSILON

---
## DECLARATIONS AND AFFECTATIONS
---

- [ ] **DECLARATION_VAR** = (TYPES . NOM) . (EPSILON | "=" . EXPRESSION)

- [ ] **DECLARATION_FONCT** = (TYPES . NOM . "(" . PARAMOUVIDE . ")" ."{" .CORPS. "}" )

- [ ] **APPEL_FONC** = NOM_FONC . "(" . EXPRESSION . ")"

- [ ] **AFFECTATION_VAR** = NOM . "=" . EXPRESSION
 
---
## EXPRESSIONS
---

- [ ] **EXPRESSION** = VALEUR_IMMEDIATE . EXPRESSION_PLUS | VARIABLE . EXPRESSION_PLUS
| APPEL_FONC . EXPRESSION_PLUS

- [ ] **EXPRESSION_PLUS** = OPERATORS . EXPRESSION | EPSILON

---
 ## PARAMETERS
---

- [ ] **PARAM** = (TYPES.NOM.PARAM_STAR)

- [ ] **PARAMOVIDE** = PARAM | EPSILON

- [ ] **PARAM_STAR** = ("," . PARAM) | EPSILON

---
## CONDITIONS
---

- [ ] **OPERANDE** = (VAR | VALEUR | EXPRESSION | APPEL_FONTION) 

- [ ] **CONDITION** = (OPERANDE . CONSTANTE_COMP . OPERANDE) | OPERANDE

- [ ] **CONDITIONS** = CONDITION | "!" . CONDITIONS | CONDITIONS_PLUS

- [ ] **CONDITIONS_PLUS** = "&&" . CONDITIONS | "||" . CONDITIONS | EPSILON

---
## LOOPS
---

- [ ] **LOOP** = FOR_LOOP | WHILE_LOOP | DO_LOOP 

- [ ] **FOR_LOOP** = "for" . "(" . (VAR | DECLARATION_VAR) . ";" . CONDITIONS . ";" . INCR . ")" . "{" . CORPS_LOOP . "}"

- [ ] **WHILE_LOOP** = "while(" . CONDITIONS . "){" . CORPS_LOOP . "}"

- [ ] **DO_LOOP** = "do" . "{" . CORPS_LOOP . "}" . "while" . "(" . CONDITIONS . ")" . ";"








Comments variables : // or /*
Comments NON viables : / / or / *